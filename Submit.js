import React from "react";
import { View, Button, Alert } from "react-native";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      noOfPeoples: "",
      fromDate: "",
      toDate: "",
      time: "",
    };
  }
  submit() {
    Alert.alert("Submitted Successfully", "Thank You", [{ text: "Ok" }]);
  }
  render() {
    return (
      <Button
        title="Submit"
        onPress={() => {
          submit();
        }}
      />
    );
  }
}
