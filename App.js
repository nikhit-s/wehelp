import MapView, {
  Callout,
  Circle,
  Marker,
  PROVIDER_GOOGLE,
} from "react-native-maps";
import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import axios from "axios";

import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Picker,
  Button,
  Alert,
  ScrollView,
} from "react-native";
import Date from "./Date";

export default function App() {
  const [PeoplesCount, setPeoples] = useState("22");
  const [FromDate, setFDate] = useState();
  const [ToDate, setTDate] = useState();
  const [Time, setSelectedValue] = useState("Morning");
  const [pin, setPin] = useState({
    latitude: 9.98367193275229,
    longitude: 76.29565436393023,
  });
  const [Latitude, setLat] = useState({ latitude: 9.98367193275229 });
  const [Longitude, setLong] = useState({ longitude: 76.29565436393023 });

  const submitHandler = (e) => {
    e.preventDefault();
    Alert.alert(
      "Submitted Successfully",
      "For future use contact 7012429924",

      [{ text: "Ok" }]
    );

    const objt = { PeoplesCount, FromDate, ToDate, Time, Latitude, Longitude };

    axios
      .post(
        "https://sheet.best/api/sheets/f6795737-d40a-4c0e-9dbf-83bced44c8d8",
        objt
      )
      .then((response) => {
        console.log(response);
      });
  };

  return (
    <View>
      <View style={styles.container}>
        <ScrollView>
          <View>
            <MapView
              provider={PROVIDER_GOOGLE} // remove if not using Google Maps
              style={styles.map}
              region={{
                latitude: 9.98367193275229,
                longitude: 76.29565436393023,
                latitudeDelta: 0.015,
                longitudeDelta: 0.0121,
              }}
            >
              <Marker
                coordinate={{
                  latitude: 9.98367193275229,
                  longitude: 76.29565436393023,
                }}
                pinColor="black"
                draggable={true}
                onDragStart={(e) => {
                  console.log("Drag start", e.nativeEvent.coordinate);
                }}
                onDragEnd={(e) => {
                  setPin({
                    latitude: e.nativeEvent.coordinate.latitude,
                    longitude: e.nativeEvent.coordinate.longitude,
                  });
                  setLat({ latitude: e.nativeEvent.coordinate.latitude });
                  setLong({ longitude: e.nativeEvent.coordinate.longitude });
                  console.log(pin);
                }}
              >
                <Callout>
                  <Text>Drag and Drop on Location</Text>
                </Callout>
              </Marker>
              <Circle center={pin} radius={50} />
            </MapView>
          </View>

          <View style={{ flexDirection: "column" }}>
            <Text
              style={{
                fontSize: 20,
                textAlign: "center",
                flex: 1,
                fontWeight: "bold",
                color: "#283f3b",
              }}
            >
              Give Details
            </Text>
            <Text style={styles.heading}> Enter number of peoples</Text>
            <TextInput
              keyboardType="numeric"
              style={styles.input}
              placeholder=" Number of peoples"
              onChangeText={(val) => setPeoples(val)}
            />

            <Text style={styles.heading}> From</Text>
            <View>
              <Date />
            </View>

            <Text style={styles.heading}> To</Text>
            <View>
              <Date />
            </View>

            <Text style={styles.heading}> Location Coordinates</Text>
            <View>
              <Text> Latitude: {Object.values(Latitude)}</Text>
              <Text> Longtitude: {Object.values(Longitude)}</Text>
            </View>

            <View>
              <Text style={styles.heading}> Select Time</Text>
              <Picker
                Time={Time}
                style={styles.input}
                onValueChange={(itemValue, itemIndex) =>
                  setSelectedValue(itemValue)
                }
              >
                <Picker.Item label="Only Morning" value="Morning" />
                <Picker.Item label="Only Afternoon" value="Afternoon" />
                <Picker.Item label="Only Dinner" value="Dinner" />
                <Picker.Item
                  label="Both Morning and Afternoon"
                  value="Both Morning and Afternoon"
                />
                <Picker.Item
                  label="Both Monring and Dinner"
                  value="Both Monring and Dinner"
                />
                <Picker.Item
                  label="Both Afternoon and Dinner"
                  value="Both Afternoon and Dinner"
                />
                <Picker.Item label="Full Day" value="FullDay" />
              </Picker>
            </View>

            <Button title="Submit" onPress={submitHandler} />
          </View>
        </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#daffed",
  },
  map: {
    flex: 2,
    width: 500,
    height: 400,
    alignItems: "stretch",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingVertical: 50,
    paddingHorizontal: 30,
  },
  input: {
    borderWidth: 1,
    borderColor: "#777",
    padding: 0,
    margin: 4,
    width: 300,
    marginBottom: 10,
  },
  heading: {
    fontSize: 15,
    fontWeight: "bold",
    color: "#473198",
  },
});
