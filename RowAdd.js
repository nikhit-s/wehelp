import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import axios from "axios";

function App() {
  const [PeoplesCount, setName] = useState("588985");
  const [FromDate, setAge] = useState("12/11/2021");
  const [ToDate, setSalary] = useState("21/10/2020");
  const [Time, setHobby] = useState("dfgfdgfdfdf");
  const [Latitude, setLat] = useState("665");
  const [Longitude, setLong] = useState("66");

  const handleSubmit = (e) => {
    e.preventDefault();

    const objt = { PeoplesCount, FromDate, ToDate, Time, Latitude, Longitude };

    axios
      .post(
        "https://sheet.best/api/sheets/f6795737-d40a-4c0e-9dbf-83bced44c8d8",
        objt
      )
      .then((response) => {
        console.log(response);
      });
  };

  return (
    <View style={styles.container}>
      <Button title="Click" onPress={handleSubmit} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
export default App;
