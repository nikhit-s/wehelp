import React, { Component } from "react";
import DatePicker from "react-native-datepicker";
import { View, Text } from "react-native";

export default class MyDatePicker extends Component {
  constructor(props) {
    super(props);
    this.state = { date: "" };
  }

  render() {
    return (
      <View>
        <DatePicker
          style={{ borderColor: "#777", width: 300 }}
          date={this.state.date}
          mode="date"
          placeholder="Select Date"
          format="YYYY-MM-DD"
          minDate="2021-01-01"
          maxDate="2023-01-01"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              position: "absolute",
              left: 0,
              top: 4,
              marginLeft: 0,
            },
            dateInput: {
              marginLeft: 36,
            },
          }}
          onDateChange={(date) => {
            this.setState({ date: date });
          }}
        />
      </View>
    );
  }
}
